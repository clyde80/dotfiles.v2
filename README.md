Dotfiles
======

![Photo](https://image.ibb.co/fa1Nn6/2017_12_25_194745_1920x1080_scrot.png)

Programs Used
------
* i3-gaps
* XFCE4-Terminal
* [Polybar](https://github.com/jaagr/polybar)
* [Pywal](https://github.com/dylanaraps/pywal)
* [CMUS](https://cmus.github.io/)
* [Cava](https://github.com/karlstav/cava)

Other Information
------
* Color scripts are in the "color_scripts" directory.
* Icons for the /scripts/lock script are located in the /scripts/icons directory.
* Scripts used in Polybar can be found in the /scripts directory.
* To change terminal padding for xfce4-terminal, in the /gtk-3.0/gtk.css file, change the `padding` value for `VteTerminal, vte-terminal`.

i3 Layout Files
------
* In the /i3/layouts directory, there are some layout files for i3. To use them, open a terminal and type `i3-msg "workspace <workspace number>; append_layout <full path to file>"`. Example: `i3-msg "workspace 1; append_layout /home/user/.config/i3/layouts/ufetch.json"`.
* I'd suggest editing the layout files before attempting to use them, unless you only intend to use them with xfce4-terminal. For help editing layout files, consult the "Editing layout files" section of [Layout saving in i3](https://i3wm.org/docs/layout-saving.html).